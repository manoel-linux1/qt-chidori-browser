#include <QApplication>
#include <QtWebEngineWidgets>
#include <QStyleFactory>

int main(int argc, char *argv[]) {
    // Create a Qt application object
    QApplication a(argc, argv);

    qDebug() << " // Application started // ";

    // Create a Qt WebEngineProfile with a unique name
    qDebug() << " // Creating WebEngineProfile with a unique name // ";
    QWebEngineProfile *profile = new QWebEngineProfile("Cookies");

    // Create a Qt WebEnginePage using the profile
    qDebug() << " // Creating WebEnginePage using the profile // ";
    QWebEnginePage *page = new QWebEnginePage(profile);

    // Disable JavaScript
    qDebug() << " // Disabling JavaScript // ";
    page->settings()->setAttribute(QWebEngineSettings::JavascriptEnabled, false);

    // Set the User-Agent
    qDebug() << " // Setting User-Agent // ";
    QString mobileUserAgent = "Mozilla/1.0 (Android 14; Mobile; rv:1.0) Gecko/1.0 Firefox/1.0";
    page->profile()->setHttpUserAgent(mobileUserAgent);

    // Create a Qt WebEngineView and set the page directly in the constructor
    qDebug() << " // Creating WebEngineView and setting the page // ";
    QWebEngineView *view = new QWebEngineView(page);

    // Set the URL of the desired website (https://api.invidious.io)
    qDebug() << " // Setting URL to: https://invidious.einfachzocken.eu // ";
    view->setUrl(QUrl("https://invidious.einfachzocken.eu"));

    // Set the zoom factor if needed
    qDebug() << " // Setting Zoom Factor to 1.2 // ";
    view->setZoomFactor(1.2);

    // Set a dark style
    qDebug() << " // Setting Dark Style // ";
    qApp->setStyle(QStyleFactory::create("Fusion"));
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
    darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);
    qApp->setPalette(darkPalette);

    // Set the application style
    qDebug() << " // Setting Application Style // ";
    qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");

    // Display the window containing the WebView
    qDebug() << " // Displaying WebView window // ";
    view->show();

    // Start the event loop of the application
    qDebug() << " // Starting the application event loop // ";
    return a.exec();
}