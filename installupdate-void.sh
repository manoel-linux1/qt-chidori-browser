#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

sudo rm /usr/bin/qt-yt-invidious

sudo rm /usr/share/applications/qt-yt-invidious.desktop

clear

sudo xbps-install -Sy && sudo xbps-install -Syu

sudo xbps-install -S qt6-webengine-devel qt6-wayland-devel qt6-webengine meson qt6-wayland

clear

export CFLAGS="-march=native -Ofast -pipe"

export CXXFLAGS="${CFLAGS}"

export LDFLAGS="-Wl,--as-needed -Wl,-Ofast ${CFLAGS}"

meson setup build

cd build

ninja -j$(nproc)

sudo cp qt-yt-invidious /usr/bin/

cd ..

sudo cp desktop/qt-yt-invidious.desktop /usr/share/applications/

sudo xbps-remove -R meson qt6-webengine-devel qt6-wayland-devel

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
