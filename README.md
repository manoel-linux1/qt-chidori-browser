# qt-yt-invidious

Version: 0.0.0.

required to have Streaming SIMD Extensions 3.

Compatible with X11/XORG and Wayland and Framebuffer.

Working on musl/wayland without freezing after this commit [https://gitlab.com/manoel-linux1/qt-yt-invidious/-/commit/009125c0316f8a77e2d4d4d9e69bc773afe9cb08] up to [https://gitlab.com/manoel-linux1/qt-yt-invidious/-/commit/1b10c162229a0c38dda05e9c8f7f3c85a583c728]. The freezing issue has been resolved. and it's now operating on musl/wayland without any freezing.

The Cookies was implemented in this commit [https://gitlab.com/manoel-linux1/qt-yt-invidious/-/commit/183d6c0898984ff61b83a41252ae4e246778701f]. Cookies are stored in `~/.local/share/qt-yt-invidious/QtWebEngine/`

## How to build

It is necessary to have some dependencies to compile: (qt6-webengine) (meson) (qt6-wayland). For Void-Linux. it is not necessary to install dependencies. as the dependencies are included in installupdate-void.sh

```bash
git clone https://gitlab.com/manoel-linux1/qt-yt-invidious.git
cd qt-yt-invidious
chmod a+x installupdate.sh/(installupdate-void.sh) ## For Void-Linux glibc/musl
./installupdate.sh/(installupdate-void.sh) ## For Void-Linux glibc/musl
qt-yt-invidious
```

to compile without the installupdate.sh/installupdate-void.sh

```bash
git clone https://gitlab.com/manoel-linux1/qt-yt-invidious.git
cd qt-yt-invidious
meson setup build
cd build
ninja -j$(nproc)
./qt-yt-invidious
```

Note that the qt-yt-invidious is only compatible with Qt6.

# Functions that are not available

-

## Screenshot

<img src="https://gitlab.com/manoel-linux1/qt-yt-invidious/-/raw/master/screenshot.png" align="center" width="720"/>
